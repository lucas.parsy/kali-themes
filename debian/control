Source: kali-themes
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders:
 Daniel Ruiz de Alegría <daniruiz@kali.org>,
 Raphaël Hertzog <raphael@offensive-security.com>,
Build-Depends:
 debhelper-compat (= 13),
 librsvg2-bin,
 optipng,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://gitlab.com/kalilinux/packages/kali-themes.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/kali-themes

Package: kali-themes-common
Architecture: all
Depends:
 kali-wallpapers-2024,
 ${misc:Depends},
Suggests:
 gtk2-engines-pixbuf,
 kali-wallpapers-2019.4,
 kali-wallpapers-2020.4,
 kali-wallpapers-2021.4,
 kali-wallpapers-2022,
 kali-wallpapers-2023,
 kali-wallpapers-legacy,
 librsvg2-common,
Breaks:
 desktop-base (<< 10.0.3+kali2),
 gnome-theme-kali (<< 2019.4),
 kali-desktop-kde (<< 2021.4.1),
 kali-menu (<< 2021.3.2),
Replaces:
 desktop-base (<< 10.0.3+kali2),
 gnome-theme-kali (<< 2019.4),
 kali-menu (<< 2021.3.2),
Provides:
 gnome-theme-kali,
Description: Kali Themes (data files)
 This package contains multiple themes for kali. It includes graphical toolkit
 themes, icon themes, color schemes, desktop backgrounds and more.
 .
 This package only contains the actual files, it doesn't change any system
 setting and doesn't enable any Kali theme by default.

Package: kali-desktop-base
Architecture: all
Depends:
 kali-themes-common (= ${source:Version}),
 ${misc:Depends},
Description: Kali version of Debian's desktop-base package
 This empty package provides hooks into the various alternatives defined by
 Debian's desktop-base to provide consistent Kali branding through the whole
 distribution.

Package: kali-themes
Architecture: all
Depends:
 fonts-cantarell,
 fonts-firacode,
 gtk2-engines-pixbuf,
 kali-themes-common (= ${source:Version}),
 librsvg2-common,
 plymouth-label,
 ${misc:Depends},
Breaks:
 gnome-shell (>= 46~),
 gnome-shell (<< 45~),
 kali-defaults (<< 2019.4.0),
 kali-desktop-xfce (<< 2020.2.18),
 kali-menu (<< 2019.4.4),
Replaces:
 kali-defaults (<< 2019.4.0),
Description: Configure all desktops to use the Kali theme
 Installing this package should configure most desktops to use the Kali theme
 by default.
 .
 The actual artwork files are provided by kali-themes-common but this package
 provides configuration files and other settings for each desktop so that
 they use the Kali theme by default.

Package: gnome-theme-kali
Section: oldlibs
Architecture: all
Depends:
 kali-themes,
 ${misc:Depends},
Description: Transitional package to install kali-themes
 All the theme related files have been moved to kali-themes.
 .
 This dummy package can be safely removed once kali-themes is installed on the
 system.

Package: kali-themes-mobile
Architecture: all
Depends:
 kali-themes (= ${source:Version}),
 kali-wallpapers-mobile-2023,
 adw-gtk3-kali,
 ${misc:Depends},
Description: Configure Phosh desktop to use the Kali theme
 Installing this package should configure Phosh desktop to use the Kali theme
 by default.

Package: kali-themes-purple
Architecture: all
Depends:
 kali-themes (= ${source:Version}),
 ${misc:Depends},
Breaks:
 kali-themes (<< 2023.3.1),
Replaces:
 kali-themes (<< 2023.3.1),
Description: Configure all desktops to use the Kali Purple theme
 Installing this package should configure most desktops to use the Kali Purple
 theme by default.
 .
 The actual artwork files are provided by kali-themes-common but this package
 provides configuration files and other settings for each desktop so that
 they use the Kali Purple theme by default.
